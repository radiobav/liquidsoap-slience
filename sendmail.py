import smtplib
from email.message import EmailMessage
from datetime import datetime

dt = datetime.now()
dateString = dt.ctime()

mailText = open('./mail.txt', 'r')
msg = EmailMessage()
msg.set_content(mailText.read())

msg['Subject'] = "{0} -- Stream error".format(dateString)
msg['From'] = '<from-adress>'
msg['To'] = ['<recipient-adress-1>', '<recipient-adress-2>', '<recipient-adress-N>']

smtp = smtplib.SMTP_SSL('<hostname>', 465)
smtp.login('<username>', '<password>')
smtp.send_message(msg)
smtp.quit()